angular.module('myApp', [
		'ui.router',
		'ngAnimate'
	])
.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('home', {
			url: '/home',
			templateUrl: 'templates/home.html'
		})
		.state('app', {
			/* === [QUESTION 1]: 
				"Make the state url option have 2 parameters with the second one optional..." */
			url: "/app/{page:int}/:detail",
			views: {
				/* === [QUESTION 1]: 
					"Use ui-route to create at least 3 pages that share a single state for loading them. To achieve this, have state url parameters by dynamic and load views/controllers based on those parameters." */
				'': {
					templateUrl: function ($stateParams){
						return 'templates/page' + $stateParams.page + '.html';
					},
					controllerProvider: function($stateParams) {
					  var ctrlName = "page" + $stateParams.page + "Controller";
					  return ctrlName;
					}
				},
				/* === [QUESTION 1]: 
					"... so that one of the pages can load additional information inside another ui-view based on that parameter."  */
				'detail': {
					templateUrl: 'templates/detail.html',
					controller: 'detailController'
				}
			}
		});
		
	$urlRouterProvider.otherwise('/home');
	
});
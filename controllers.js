angular.module('myApp')

// CONTROLLER: Page 1
.controller('page1Controller', function ($scope, $stateParams, generalService, personService) {
	$scope.general = generalService;
	$scope.person = personService.getPerson($stateParams.page);

	/* === [QUESTION 7] === */
	$scope.sum = {
		one: 0,
		two: 0,
		three: 0,
		total: 0
	};

	var lastTotal = $scope.sum.total;
	var rateOne, rateTwo, rateThree, rateTotal;
	$scope.updateTotal = function() {
		$scope.sum.total = $scope.sum.one + $scope.sum.two + $scope.sum.three;
		rateOne = $scope.sum.one / $scope.sum.total;
		rateTwo = $scope.sum.two / $scope.sum.total;
		rateThree = $scope.sum.three / $scope.sum.total;

		lastTotal = $scope.sum.total;
	}

	$scope.updateExpression = function() {
		if(lastTotal == 0 || $scope.sum.total == 0 || lastTotal == null){
			$scope.sum.one = Math.round($scope.sum.total / 3);
			$scope.sum.two = Math.round($scope.sum.total / 3);
			$scope.sum.three = Math.round($scope.sum.total / 3);
		} else {
			rateTotal = $scope.sum.total / lastTotal;
			
			$scope.sum.one = Math.round($scope.sum.one * rateTotal * 100) / 100;
			$scope.sum.two = Math.round($scope.sum.two * rateTotal * 100) / 100;
			$scope.sum.three = Math.round($scope.sum.three * rateTotal * 100) / 100;
		}
		lastTotal = $scope.sum.total;
	}
})

// CONTROLLER: Page 2
.controller('page2Controller', function ($scope, $stateParams, generalService, personService) {
	$scope.general = generalService;
	$scope.person = personService.getPerson($stateParams.page);
})

// CONTROLLER: Page 3
.controller('page3Controller', function ($scope, $stateParams, generalService, personService, $http) {
	$scope.general = generalService;
	$scope.person = personService.getPerson($stateParams.page);

	// Array
	$http.get('medals.json').success(function(data) {
		$scope.medals = data;
	});
})

// CONTROLLER: Detail
.controller('detailController', function($scope, $stateParams, personService) {
	if($stateParams.detail == "detail") {
		$scope.person = personService.getPerson($stateParams.page);
	} else {
		$scope.person = null;
	}
});
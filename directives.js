angular.module('myApp')

// FORMAT PHONE
.directive("formatPhone", function () {
  return {
    require: "ngModel",
    restrict: "A",
    link: function (scope, elem, attrs, ngModelCtrl) {
      // PARSER
      ngModelCtrl.$parsers.push(function (value) {

          var prevValue, nextValue;

          prevValue = value;
          nextValue = value.replace(/[^0-9]/g, ''); // Only numbers

          // (555) 555-5555 format
          if (nextValue.length >= 4 && nextValue.length <= 6) {
            nextValue = nextValue.replace(/(\d{3})(\d{3})?/, "($1) $2");
          } else if (nextValue.length >= 7 && nextValue.length <= 10) {
            nextValue = nextValue.replace(/(\d{3})(\d{3})(\d{5})?/, "($1) $2-$3");
          }

          // Render Value in View
          if (prevValue != nextValue) {
            ngModelCtrl.$setViewValue(nextValue);
          } else {
            ngModelCtrl.$render();
          }

          return value;
      });
    }
  }
})

// FORMAT CURRENCY
.directive("formatCurrency", function () {
  return {
    require: "ngModel",
    restrict: "A",
    link: function (scope, elem, attrs, ngModelCtrl) {
      // PARSER
      ngModelCtrl.$parsers.push(function (value) {

          var prevValue, nextValue;

          prevValue = value;
          nextValue = value.replace(/[^0-9]/g, ''); // Only numbers

          // $1,234 format
          if (nextValue.length > 0) {
        		nextValue = nextValue.replace(/(\d+)/, "$$$1");
          }
          if (nextValue.length > 4) {
          	nextValue = nextValue.replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
          }

          // Render Value in View
          if (prevValue != nextValue) {
            ngModelCtrl.$setViewValue(nextValue);
          } else {
						ngModelCtrl.$render();
          }

          return value;
      });
    }
  }
});
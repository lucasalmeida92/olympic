angular.module('myApp')

.factory('generalService', function () {
	return {
		visitorName: 'Visitor',
		visitorPhone: undefined
	}
})

.factory('personService', function () {
	
	var persons = [
			{
				name: 'Ryan',
				detail: {
					age: 21,
					about: 'lorem ipsum dolor sit amet.'	
				}
			},
			{
				name: 'John',
				detail: {
					age: 22,
					about: 'lorem ipsum dolor sit amet, consectetur adipisicing elit.'	
				}
			},
			{
				name: 'Bill',
				detail: {
					age: 19,
					about: 'lorem ipsum dolor sit amet.'	
				}
			}		
		];

	return {
		getPerson: function(index){
			return persons[index-1];
		}
	}
		
});